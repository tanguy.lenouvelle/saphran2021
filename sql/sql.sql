CREATE TABLE roles(
    idrole int not null auto_increment,
    nom varchar(10),
    permissions int,
    constraint pk_roles primary key (idrole)
);

CREATE TABLE users(
    iduser int not null auto_increment,
    login varchar(50),
    passwd varchar(50),
    idrole int,
    constraint pk_users primary key(iduser),
    constraint fk_idrole foreign key (idrole) references roles(idrole)
);