<?php

//https://www.youtube.com/watch?v=tbYa0rJQyoM
//https://www.youtube.com/watch?v=-iW6lo6wq1Y
//https://openclassrooms.com/fr/courses/2078536-developpez-votre-site-web-avec-le-framework-symfony2-ancienne-version/2079345-le-routeur-de-symfony2

require_once '../src/model/City.php';
require_once '../src/model/Country.php';

require_once '../src/model/DAOCity.php';
require_once '../src/model/DAOCountry.php';

require_once '../src/model/Model.php';


require_once '../src/controllers/CityController.php';

require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/DefaultController.php';
require_once '../src/controllers/UserController.php';


//echo "<pre>" . print_r($_SERVER, true) . "<pre>";

if (isset($_SERVER["PATH_INFO"])) {
    $path = trim($_SERVER["PATH_INFO"], "/");
} else {
    $path = "";
}

$fragments = explode("/", $path);

//var_dump($fragment);

$control = array_shift($fragments);
//echo "control : $control <hr>";
switch ($control) {
    case '' : { //l'url est /
            $index = new DefaultController();
            $index->show();
            break;
        }
    case "city" : {
            //echo "Gestion des routes pour city <hr>";
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                cityRoutes_get($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                cityRoutes_post($fragments);
            }
            break;
        }
    case "country" : {
            //echo "Gestion des routes pour country<hr>";
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                countryRoutes($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                countryRoutes_post($fragments);
            }     
            break;
        }
    case "users" : {
            //echo "Gestion des routes pour country<hr>";
            //calling function to prevend all hard code here
            if ($_SERVER["REQUEST_METHOD"] == "GET") {
                userGET($fragments);
            }
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                userPOST($fragments);
            }     
            break;
        }
    default : {
            $index = new DefaultController();
            $index->show();
        }
}

function cityRoutes_get($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "show" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling cityController->show <hr>";
                call_user_func_array([new CityController(), "show"], $fragments);
               
                break;
            }
        case "edit" : {
                //http://127.0.0.1:8080/city/demo/1/45?p=2
                //echo "Calling cityController->demo_test <hr>";
                //var_dump($fragments);
                call_user_func_array([new CityController(), "showEdit"], $fragments);

                break;
            }
        case "add" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling cityController->show <hr>";
                call_user_func_array([new CityController(), "showAdd"], array());
               
                break;
            } 
        case "delete" : {
                //echo "Calling cityController->del <hr>";
                call_user_func_array([new CityController(), "delete"], $fragments);
               
                break;
            }
        default : {
                $index = new DefaultController();
                $index->show();
            }
    }
}

 

function cityRoutes_post($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "edit" : {
                //http://127.0.0.1:8080/city/demo/1/45?p=2
                //echo "Calling cityController->demo_test <hr>";
                //var_dump($fragments);
                call_user_func_array([new CityController(), "doEdit"], $fragments);

                break;
            }
        case "add" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling cityController->show <hr>";
                call_user_func_array([new CityController(), "doAdd"], array());
               
                break;
            } 
        case "delete" : {
                //echo "Calling cityController->del <hr>";
                break;
            }
        default : {
                $index = new DefaultController();
                $index->show();
            }
    }
}


function countryRoutes($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "show" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling CountryController->show <hr>";
                //var_dump($fragments);   

                call_user_func_array([new CountryController(), "show"], $fragments);
               
                break;
            }
        case "All" : {
                //http://127.0.0.1:8080/city/demo/1/45?p=2
                //echo "Calling cityController->demo_test <hr>";
                //var_dump($fragments);

                call_user_func_array([new CountryController(), "All"], $fragments);

                break;
            }
        case "add" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling cityController->show <hr>";
                call_user_func_array([new CountryController(), "showAdd"], array());
               
                break;
            } 
        case "edit" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling CountryController->show <hr>";
                //var_dump($fragments);   

                call_user_func_array([new CountryController(), "showEdit"], $fragments);
               
                break;
            }  

        default : {
                $index = new DefaultController();
                $index->show();
            }
    } 
}


function countryRoutes_post($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "edit" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling CountryController->show <hr>";
                //var_dump($fragments);   

                call_user_func_array([new CountryController(), "doEdit"], $fragments);
               
                break;
            } 
        case "add" : {
                //echo "Calling cityController->del <hr>";
                call_user_func_array([new CountryController(), "doAdd"], $fragments);
               
                break;
            }
        case "delete" : {
                //echo "Calling cityController->del <hr>";
                call_user_func_array([new CountryController(), "delete"], $fragments);
               
                break;
            }
        default : {
                $index = new DefaultController();
                $index->show();
            }
    }
}


function userGET($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "login" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling CountryController->show <hr>";
                //var_dump($fragments);   

                call_user_func_array([new UserController(), "login"], $fragments);
               
                break;
            }

        default : {
                $index = new DefaultController();
                $index->show();
            }
    } 
}


function userPOST($fragments) {

    //var_dump($fragment);

    $action = array_shift($fragments);
    
    //var_dump($action);

    switch ($action) {
        case "login" : {
                //http://127.0.0.1:8080/city/show/5?p=25&a=12
                //echo "Calling CountryController->show <hr>";
                //var_dump($fragments);   

                call_user_func_array([new UserController(), "doLogin"], $fragments);
               
                break;
            }

        default : {
                $index = new DefaultController();
                $index->show();
            }
    } 
}