<?php


require_once '../src/model/Country.php';
require_once '../src/model/DAOCountry.php';
require_once '../src/utils/SingletonDataBase.php';
require_once '../src/utils/Renderer.php';
require_once '../src/controllers/CountryController.php';
require_once '../src/controllers/DefaultController.php';

?>



<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8" />
  	<script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<link type="text/javascript" href="js/bootstrap.min.js">
  	<link rel="stylesheet" href="/public_html/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/public_html/css/style.css">
  	<link rel="icon" type="png/image" href="favicon.png"/>
  	<link rel="icon" type="image/x-icon" href="img/logo.PNG" /><link rel="shortcut icon" type="image/x-icon" href="img/logo.PNG" />
  	<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
    </head>
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><ion-icon name="globe-outline"></ion-icon> World Data</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
        <br>    
     
         <div class="container">  
        
        <form method="POST">
           
          <div class="form-group">
            <label>id:</label>
            <input class="form-control" value="<?php $city = getCity_Id() ?>" >
            
          </div>
            
          <div class="form-group">
            <label for="exampleInputPassword1">Nom:</label>
            <input class="form-control" value="<?php $city = getName() ?>" >
          </div>
          
          <div class="form-group">
            <label for="exampleInputPassword1">Code:</label>
            <input class="form-control" value="<?php $city = getCityCode() ?>" >
          </div>
            
          <div class="form-group">
            <label for="exampleInputPassword1">Population:</label>
            <input class="form-control" type="number" value="<?php $city = getPopulation() ?>">
          </div>
          
            
          <button type="submit" class="btn btn-success">Valider</button>
          <button type="submit"  class="btn btn-danger">reset</button>
          
          <button type="submit" class="btn btn-dark" style="height:500px; width:200px">Back</button>
          
          
        </form>
        
         </div>
         

     </body>
</html>

