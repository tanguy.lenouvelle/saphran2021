<!DOCTYPE html>
<html lang="fr">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8" />
  	<script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<link type="text/javascript" href="/public_html/js/bootstrap.min.js">
  	<link rel="stylesheet" href="/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/css/style.css">
  	<link rel="icon" type="png/image" href="favicon.png"/>
  	<link rel="icon" type="image/x-icon" href="img/logo.PNG" /><link rel="shortcut icon" type="image/x-icon" href="img/logo.PNG" />
  	<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><ion-icon name="globe-outline"></ion-icon> World Data</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    
<div class="container">
			<br>
			<br>
			<h1><center>Detail pour le pays : </center></h1>

<center>
<img src="<?= $country->getImage2(); ?>" alt="..." height="64">
</center>
                        
                        
<hr class="my-4">
<div class="row">
    <div class="col" style="font-weight:bold;">
      id
    </div>
    <div class="col">
      <?= $country->getCountry_Id(); ?>
    </div>
</div>
<hr class="my-4">
<div class="row">
    <div class="col" style="font-weight:bold;">
      Nom
    </div>
    <div class="col">
       <?= $country->getName(); ?>
    </div>
</div>
<hr class="my-4">
<div class="row">
    <div class="col" style="font-weight:bold;">
       Continent
    </div>
    <div class="col">
      <?= $country->getContinent(); ?>
    </div>
</div>
<hr class="my-4">
<div class="row">
    <div class="col" style="font-weight:bold;">
      Capital
    </div>
    <div class="col">
      <?= $country->getCapital(); ?>
    </div>
</div>
<hr class="my-4">
<div class="row">
    <div class="col" style="font-weight:bold;">
      Population
    </div>
    <div class="col">
      <?= $country->getPopulation(); ?>
    </div>
</div>
<hr class="my-4">
<br>




<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">id</th>
      <th scope="col">nom</th>
      <th scope="col">District</th>
      <th scope="col">Population</th>
      <?php if($isLogged): ?>
      <th scope="col"><a href="/city/add/?code=<?= $country->getCode(); ?>&idcountry=<?= $country->getCountry_Id(); ?>"><ion-icon name="create"></ion-icon></a></th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
      <?php 
      foreach($cities as $city): ?>
    <tr>
      <th scope="row"></th>
      <td><?= $city[0]; ?></td>
      <td><a href="/city/show/<?= $city[0]; ?>"><?= $city[1]; ?></a></td>
      <td><?= $city[3]; ?></td>
      <td><?= $city[4]; ?></td>
      <?php if($isLogged): ?>
      <td>
      <a href="/city/edit/<?= $city[0]; ?>/?idcountry=<?= $country->getCountry_Id(); ?>" style="color: #32CD32">
        <ion-icon name="pencil-sharp"></ion-icon>
      </a>
      <a href="/city/delete/<?= $city[0]; ?>/?csrf_token=<?= $csrf_token; ?>" style="color: #FF0000">
        <ion-icon name="trash-sharp"></ion-icon>
      </a>
      </td>
      <?php endif; ?>
    </tr>
      <?php endforeach; ?>

  

  </tbody>
</table>
<br>


<?= $pagination; ?>

</div>
