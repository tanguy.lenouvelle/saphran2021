<!DOCTYPE html>
<html lang="fr">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8" />
  	<script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<link type="text/javascript" href="js/bootstrap.min.js">
  	<link rel="stylesheet" href="/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/css/style.css">
  	<link rel="icon" type="png/image" href="favicon.png"/>
  	<link rel="icon" type="image/x-icon" href="img/logo.PNG" /><link rel="shortcut icon" type="image/x-icon" href="img/logo.PNG" />
  	<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><ion-icon name="globe-outline"></ion-icon> World Data</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    

<div class="container">
<div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">

                        <form action="" method="POST">
                            
                        <div class="form-group row">
                          <label for="inputPassword" class="col-sm-2 col-form-label">id</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="id" value="<?= $city->getCityID(); ?>" readonly="">
                          </div>
                        </div>
                            
                            
                        <div class="form-group row">
                          <label for="inputPassword" class="col-sm-2 col-form-label">Nom</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" value="<?= $city->getName(); ?>" required="">
                          </div>
                        </div>
                            
                        <div class="form-group row">
                          <label for="inputPassword" class="col-sm-2 col-form-label">Code</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="code" value="<?= $city->getCountryCode(); ?>" required="">
                          </div>
                        </div>
                            
                        <div class="form-group row">
                          <label for="inputPassword" class="col-sm-2 col-form-label">District</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" name="district" value="<?= $city->getDistrict(); ?>" required="">
                          </div>
                        </div>
                            
                        <div class="form-group row">
                          <label for="inputPassword" class="col-sm-2 col-form-label">Population</label>
                          <div class="col-sm-10">
                            <input type="number" class="form-control" name="population" value="<?= $city->getPopulation(); ?>" required="">
                          </div>
                        </div>
                        <input type="hidden" value="<?= $csrf_token; ?>" name="csrf_token" id="csrf_token" required="">
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary">Valider</button>
                                <button type="reset" class="btn btn-success">reset</button>
                            </div>
                            <div class="col-sm-6 text-right">
                                <a class="btn btn-large btn-dark" href="/country/show/<?php if(isset($_GET['idcountry'])) echo $_GET['idcountry']; ?>">back</a>
                            </div>
                        </div>
                      </form>
                        
                        



                        
                    </div>

                </div>
</div>
