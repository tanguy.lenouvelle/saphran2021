<!DOCTYPE html>
<html lang="fr">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  	<meta charset="UTF-8" />
  	<script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>
  	<link type="text/javascript" href="/js/bootstrap.min.js">
  	<link rel="stylesheet" href="/css/bootstrap.min.css">
  	<link rel="stylesheet" href="/css/style.css">
  	<link rel="icon" type="png/image" href="favicon.png"/>
  	<link rel="icon" type="image/x-icon" href="img/logo.PNG" /><link rel="shortcut icon" type="image/x-icon" href="img/logo.PNG" />
  	<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><ion-icon name="globe-outline"></ion-icon> World Data</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/country/add/">Ajouter un pays</a>
      </li>
    </ul>
      <?php if($isLogged): ?> Bienvenue <?= $_SESSION['user']['login']; ?> - <a href="/users/logout.php">Déconnexion</a> <?php endif; ?>

  </div>
</nav>
    

<div class="container">
			<br>
			<br>
			<h1><center>Liste des pays en <?= $continent; ?> : </center></h1>
			<br>
			<br>


<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">id</th>
      <th scope="col">Drapeau</th>
      <th scope="col">nom</th>
      <th scope="col">code</th>
      <th scope="col">Region</th>
      <th scope="col">Capital</th>
      <th scope="col">Population</th>
      <?php if($isLogged): ?><th scope="col"><a href="/country/add/"><ion-icon name="create"></ion-icon></a></th><?php endif; ?>
    </tr>
  </thead>
  <tbody>
      <?php foreach($countries as $country):?>
    <tr>
      <th scope="row">1</th>
      <td><?= $country[0]; ?></td>
      <td>
      	<img src="<?= $country[17]; ?>" width="50" height="50">
      </td>
      <td>
      	<a href="/country/show/<?= $country[0]; ?>">
      		<?= $country[2]; ?>
  		</a>
  	  </td>
      <td><?= $country[1]; ?></td>
      <td><?= $country[4]; ?></td>
      <td><?= $country[14]; ?></td>
      <td><?= $country[7]; ?></td>
      <?php if($isLogged): ?>
      <td>
      <a href="/country/edit/<?= $country[0]; ?>" style="color: #32CD32">
        <ion-icon name="pencil-sharp"></ion-icon>
      </a>
      <a href="/country/delete/<?= $country[0]; ?>/?csrf_token=<?= $csrf_token; ?>" style="color: #FF0000">
        <ion-icon name="trash-sharp"></ion-icon>
      </a>
      </td>
      <?php endif; ?>
    </tr>
    <?php endforeach; ?>
    
 


  </tbody>
</table>
<br>

<?= $pagination; ?>













</div>