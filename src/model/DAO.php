<?php
require_once'Model.php';

abstract class DAO{
    
      /** @var $cnx PDO */
    protected $cnx;
    
    public function __construct($cnx) {
        $this->cnx=$cnx;
    }
    
    abstract public function find($id);
    
    abstract public function save($entity);
            
    abstract public function update($entity);
    
    abstract public function remove($entity);
    
    abstract public function findAll() ;
    
    abstract public function count() ;
}

