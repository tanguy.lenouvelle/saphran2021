<?php

require_once '../src/model/Model.php';


class City extends Model {
    
    protected $City_Id;
    protected $Name;
    protected $CountryCode;
    protected $District;
    protected $Population;
    
    /**
     * City constructor
     * @param array $data
     */
    public function __construct(array $data=NULL) {
        
        parent::__construct();
        /* $this->City_Id=$data["City_Id"];
        $this->Name=$data["Name"];
        $this->CountryCode=$data["CountryCode"];
        $this->District=$data["District"];
        $this->Population=$data["Population"]; */

     
    }

    /**
     * @return mixed
     */
    public function getCityId()
    {
        return $this->City_Id;
    }

    /**
     * @param mixed $City_Id
     */
    public function setCityId($City_Id)
    {
        $this->City_Id = $City_Id;
    }
    function getCity_Id() {
        return $this->City_Id;
    }


    function setCity_Id($City_Id): void {
        $this->City_Id = $City_Id;
    }


        /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->CountryCode;
    }

    /**
     * @param mixed $CountryCode
     */
    public function setCountryCode($CountryCode)
    {
        $this->CountryCode = $CountryCode;
    }
    function getDistrict() {
        return $this->District;
    }

    function getPopulation() {
        return $this->Population;
    }

    function setDistrict($District): void {
        $this->District = $District;
    }

    function setPopulation($Population): void {
        $this->Population = $Population;
    }


}