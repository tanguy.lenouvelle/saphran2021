<?php
require_once 'User.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DAOUser
 *
 * @author student
 */
class DAOUser extends DAO{
    //put your code here
    
    public function login($email, $passwd) {        
        
        $SQL = "SELECT COUNT(*) FROM users WHERE login = :login AND passwd = :passwd";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("login", $email);
        $preparedStatement->bindValue("passwd", $passwd);
        
        $preparedStatement->execute();
        
        $count = $preparedStatement->fetch();
        $c = $count[0];
      
        if($c == 1){
            return true;
        }
        return false;
        
    }
    


    public function find($id) {
        
    }

    public function findAll() {
        
    }

    public function remove($entity) {
        
    }

    public function save($entity) {
        $SQL = "INSERT INTO users SET login = :login, passwd = :passwd, idrole = :idrole";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("login", $entity->getLogin());
        $preparedStatement->bindValue("passwd", $entity->getPasswd());
        $preparedStatement->bindValue("idrole", $entity->getRole());
        $preparedStatement->execute();
    }

    public function update($entity) {
        
    }

    public function count() {
        
    }

}
