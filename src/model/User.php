<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author student
 */
class User extends Model {
    //put your code here
    protected $login;
    protected $passwd;
    protected $role;
    
    public function __construct() {
        
    }
    
    function getLogin() {
        return $this->login;
    }

    function getPasswd() {
        return $this->passwd;
    }

    function getRole() {
        return $this->role;
    }

    function setLogin($login): void {
        $this->login = $login;
    }

    function setPasswd($passwd): void {
        $this->passwd = $passwd;
    }

    function setRole($role): void {
        $this->role = $role;
    }


}
