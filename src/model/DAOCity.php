<?php
require_once 'DAO.php';
require_once 'City.php';
require_once 'Country.php';

class DAOCity extends DAO{
    
    protected $cnx;
    
    public function __construct($cnx) {
        $this->cnx=$cnx;
    }
    
    public function count(): int{
        $SQL = "SELECT COUNT(*) FROM city";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->execute();
        
        $count = $preparedStatement->fetch();
        $c = $count[0];
        
        return $c;  
    }
    
    public function find($id) : City{
        $SQL = "SELECT * FROM city WHERE City_Id = :id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("id", $id);
        $preparedStatement->execute();
        
        $city = $preparedStatement->fetchObject("City");
         
        return $city; 
    }
    
     public function findAllByCountryID($id, $page, $limit){

        $id = intval($id);
        
        $SQL = "SELECT * FROM country WHERE Country_Id=:Country_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Country_Id", $id);
        $preparedStatement->execute();

        $country = $preparedStatement->fetch(PDO::FETCH_ASSOC);
        

        $SQL = "SELECT * FROM city WHERE CountryCode = :countrycode LIMIT $page, $limit";
        $preparedStatements = $this->cnx->prepare($SQL);
        $preparedStatements->bindValue("countrycode", $country["Code"]);
        $preparedStatements->execute();
        $cities = $preparedStatements->fetchAll();
         
        
        return $cities; 
    }
    
    
     public function countByCountryID($id){


        $id = intval($id);
        
        $SQL = "SELECT * FROM country WHERE Country_Id=:Country_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Country_Id", $id);
        $preparedStatement->execute();

        $country = $preparedStatement->fetch(PDO::FETCH_ASSOC);
        

        $SQL = "SELECT City_Id FROM city WHERE CountryCode = :countrycode";
        $preparedStatements = $this->cnx->prepare($SQL);
        $preparedStatements->bindValue("countrycode", $country["Code"]);
        $preparedStatements->execute();
        $count = $preparedStatements->rowCount();
         
        
        return $count; 
    }
    
    public function getCityByName($name) : City{
        $SQL = "SELECT * FROM city WHERE Name = :Name";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Name", $name);
        $preparedStatement->execute();
        
        $city = $preparedStatement->fetchObject("City");
         
        return $city; 
    }

    public function findAll() {
        $SQL = "SELECT * FROM city";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->execute();
        
        $cities = $preparedStatement->fetchAll();
         
        return $cities;   
    }

    public function remove($id) {
        $SQL = "DELETE FROM city WHERE City_Id = :City_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("City_Id", $id);
        $preparedStatement->execute();     
    }

    public function save($entity) : void {
        $SQL = "INSERT INTO city SET Name = :Name, CountryCode = :CountryCode, District = :District, Population = :Population";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Name", $entity->getName());
        $preparedStatement->bindValue("CountryCode", $entity->getCountryCode());
        $preparedStatement->bindValue("District", $entity->getDistrict());
        $preparedStatement->bindValue("Population", $entity->getPopulation());
        $preparedStatement->execute();
        
    }

    public function update($entity) {
        
        $SQL = "UPDATE city SET Name = :Name, CountryCode = :CountryCode, District = :District, Population = :Population WHERE City_Id = :City_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("City_Id", $entity->getCityId());
        $preparedStatement->bindValue("Name", $entity->getName());
        $preparedStatement->bindValue("CountryCode", $entity->getCountryCode());
        $preparedStatement->bindValue("District", $entity->getDistrict());
        $preparedStatement->bindValue("Population", $entity->getPopulation());
        if($preparedStatement->execute()){
            return true;
        }
    }

}

