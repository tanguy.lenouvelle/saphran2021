<?php

require_once 'DAO.php';
require_once 'Country.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DAOCountry extends DAO{
    /** @var $cnx PDO */

    protected $cnx;

    public function __construct($cnx) {
        $this->cnx=$cnx;
    }

    public function find($id) : Country {
        // renvoie  a partir d'un id

        try{
            $cnx = new PDO("mysql:host=127.0.0.1;dbname=worlddb", "worlddbuser", "123+aze");

            $SQL = "SELECT * FROM country WHERE Country_Id = :id";

            $preparedStatement = $cnx->prepare($SQL);
            $preparedStatement->bindValue("id", $id);
            $preparedStatement->execute();

            $country = $preparedStatement->fetchObject("Country");

            return $country;

        } catch (Exception $e) {
            echo "<pre>" . print_r($e, true) ."</pre>";
        }
    }

    public function save($entity) : void {
        $SQL = "INSERT INTO country SET Code = :Code, Name = :Name, Continent = :Continent, Region = :Region, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy, GNP = :GNP, GNPOld = :GNPOld, LocalName = :LocalName, GovernmentForm = :GovernmentForm, HeadOfState = :HeadOfState, Capital = :Capital, Code2 = :Code2, Image1 = :Image1, Image2 = :Image2";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Code", $entity->getCode());
        $preparedStatement->bindValue("Name", $entity->getName());
        $preparedStatement->bindValue("Continent", $entity->getContinent());
        $preparedStatement->bindValue("Region", $entity->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $entity->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $entity->getIndepYear());
        $preparedStatement->bindValue("Population", $entity->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $entity->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $entity->getGNP());
        $preparedStatement->bindValue("GNPOld", $entity->getGNPOld());
        $preparedStatement->bindValue("LocalName", $entity->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $entity->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $entity->getHeadOfState());
        $preparedStatement->bindValue("Capital", $entity->getCapital());
        $preparedStatement->bindValue("Code2", $entity->getCode2());
        $preparedStatement->bindValue("Image1", $entity->getImage1());
        $preparedStatement->bindValue("Image2", $entity->getImage2());
        $preparedStatement->execute();

    }

    public function update($entity) {
        $SQL = "UPDATE country SET Code = :Code, Name = :Name, Continent = :Continent, Region = :Region, SurfaceArea = :SurfaceArea, IndepYear = :IndepYear, Population = :Population, LifeExpectancy = :LifeExpectancy, GNP = :GNP, GNPOld = :GNPOld, LocalName = :LocalName, GovernmentForm = :GovernmentForm, HeadOfState = :HeadOfState, Capital = :Capital, Code2 = :Code2, Image1 = :Image2";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Code", $entity->getCode());
        $preparedStatement->bindValue("Name", $entity->getName());
        $preparedStatement->bindValue("Continent", $entity->getContinent());
        $preparedStatement->bindValue("Region", $entity->getRegion());
        $preparedStatement->bindValue("SurfaceArea", $entity->getSurfaceArea());
        $preparedStatement->bindValue("IndepYear", $entity->getIndepYear());
        $preparedStatement->bindValue("Population", $entity->getPopulation());
        $preparedStatement->bindValue("LifeExpectancy", $entity->getLifeExpectancy());
        $preparedStatement->bindValue("GNP", $entity->getGNP());
        $preparedStatement->bindValue("GNPOld", $entity->getGNPOld());
        $preparedStatement->bindValue("LocalName", $entity->getLocalName());
        $preparedStatement->bindValue("GovernmentForm", $entity->getGovernmentForm());
        $preparedStatement->bindValue("HeadOfState", $entity->getHeadOfState());
        $preparedStatement->bindValue("Capital", $entity->getCapital());
        $preparedStatement->bindValue("Code2", $entity->getCode2());
        $preparedStatement->bindValue("Image1", $entity->getImage1());
        $preparedStatement->bindValue("Image2", $entity->getImage2());
        $preparedStatement->execute();
    }


    public function remove($id) {
        $SQL = "DELETE FROM Country WHERE Country_Id = :Country_Id";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Country_Id", $id);
        $preparedStatement->execute();     
    }
    
    public function findAll() {
        $SQL = "SELECT * FROM country";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->execute();

        $countries = $preparedStatement->fetchAll();

        return $countries;
    }
    
    public function findAllByContinent($continent, $page, $limit){
        $SQL = "SELECT * FROM country WHERE Continent = :continent LIMIT $page, $limit";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("continent", $continent);

        $preparedStatement->execute();

        $countries = $preparedStatement->fetchAll();

        return $countries;  
    }
        
    public function countByContinent($continent){
        $SQL = "SELECT * FROM country WHERE Continent = :continent";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("continent", $continent);

        $preparedStatement->execute();

        $count = $preparedStatement->rowCount();

        return $count;  
    }
    
    
    
    public function count(): int{
        $SQL = "SELECT COUNT(*) FROM country";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->execute();

        $count = $preparedStatement->fetch();
        $c = $count[0];

        return $c;
    }

    public function getCountryByName($name) : Country{
        $SQL = "SELECT * FROM country WHERE Name = :Name";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->bindValue("Name", $name);
        $preparedStatement->execute();

        $country = $preparedStatement->fetchObject("Country");

        return $country;
    }

    public function findCoutriesByContinent($continent){
        
    }
    
    public function get_enum_from_continent(){
        $SQL = "SELECT DISTINCT Continent FROM country";
        $preparedStatement = $this->cnx->prepare($SQL);
        $preparedStatement->execute();

        $continents = $preparedStatement->fetchAll();

        return $continents;      
    }
    
}
