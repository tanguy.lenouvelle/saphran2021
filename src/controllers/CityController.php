<?php
require_once'BaseController.php';
require_once '../src/utils/SingletonDataBase.php';
require_once '../src/utils/CsrfToken.php';


require_once '../src/model/City.php';
require_once '../src/utils/Renderer.php';
require_once '../src/model/DAOCity.php';


/**
 * Description of CityController
 *
 * @author student
 */
class CityController extends BaseController{

    /** @var City $daocity */
    private $daocity;
    use CsrfToken;
    
    public function __construct() {
        $this->daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);
    }
    
    
    public function test($alpha){
        
    }
    
    
    public function show($id){
       // trouver les infos d'une ville
        
       $city = $this->daocity->find($id);
       $csrf_token = $this->generateToken();
       $page = Renderer::render('city.php', compact("city","csrf_token"));
       echo $page;
    }
    
    public function showEdit($id){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();

       }
       // generer un token
       $csrf_token = $this->generateToken();

       // trouver les infos d'une ville
       $city = $this->daocity->find($id);
       
       
       $page = Renderer::render('city_edit.php', compact("city","csrf_token"));
       echo $page;
    }
    
    public function doEdit($id){
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();
        }

        if(!$this->check('csrf_token')){
            $page = Renderer::render('ErrorCSRF.php');
            echo $page;
        }else{
            $city = $this->daocity->find($id);
            $city->setName(htmlspecialchars($_POST['name']));
            $city->setCountryCode(htmlspecialchars($_POST['code']));
            $city->setDistrict(htmlspecialchars($_POST['district']));
            $city->setPopulation(htmlspecialchars($_POST['population']));

            $city = $this->daocity->update($city);

            header('Location: /city/edit/'.$id);
            die();    
        }


    }
    
    public function showAdd(){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();

       }
       // generer un token
       $csrf_token = $this->generateToken();
       $page = Renderer::render('city_add.php', compact('csrf_token'));
       echo $page;
    }
    
    public function doAdd(){
        
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();
        }

        if(!$this->check('csrf_token')){
            echo 'error token';
        }else{
            $city = new City();
            $city->setName(htmlspecialchars($_POST['name']));
            $city->setCountryCode(htmlspecialchars($_POST['code']));
            $city->setDistrict(htmlspecialchars($_POST['district']));
            $city->setPopulation(htmlspecialchars($_POST['population']));

            $city = $this->daocity->save($city);

            header('Location: /city/add');
            die();    
        }


    }
    
    
        
    public function delete($id){
        
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();

        }

        if(!$this->check('csrf_token')){
            echo 'error token';
        }else{
            $city = $this->daocity->find($id);

            $city = $this->daocity->remove($id);

            header("Location: {$_SERVER["HTTP_REFERER"]}");
            die();    
        }


    }



}
