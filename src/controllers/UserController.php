<?php
require_once 'BaseController.php';
require_once '../src/model/DAOUser.php';
require_once '../src/utils/Auth.php';
require_once '../src/utils/Flash.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author student
 */
class UserController {
    //put your code here
        private $daouser;
        private $auth;
        use Flash;
            
    public function __construct() {
        $this->daouser = new DAOUser(SingletonDataBase::getInstance()->cnx);
        $this->auth = new Auth();
    }
    
    public function login(){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();
       }
       $page = Renderer::render('login.php');
       echo $page;
    }
    
     public function doLogin(){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();
       }
         $login = htmlspecialchars($_POST['email']);
         $passwd = htmlspecialchars($_POST['passwd']);
         
         if($this->daouser->login($login, $passwd)){
             $user = new User();
             $user->setLogin($login);
             $user->setPasswd($passwd);
             
             $this->auth->login($user);
             var_dump($_SESSION);
             $log = $_SESSION['user']['login'];
             
             $this->setFlash("Bienvenue {$log}");
             header('Location: /users/login');
             die();
             
         }else{
             $this->setFlash("Login ou mot de passe incorrect", "error");
             header('Location: /users/login');
             die();
         }
    }   
}
