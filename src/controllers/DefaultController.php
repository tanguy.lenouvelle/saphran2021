<?php
require_once'BaseController.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author student
 */
class DefaultController extends BaseController{
    
    
    /** @var City $daocountry */
    private $daocountry;
    
    public function __construct() {
        $this->daocountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);

    }
    
    public function show(){
       //afficher page d'accueil et continent
       $continents = $this->daocountry->get_enum_from_continent();
       $page = Renderer::render('welcome.php', compact("continents"));
       echo $page;
    }
    
    
}
