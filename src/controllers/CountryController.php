<?php

require_once 'BaseController.php';
require_once '../src/utils/Paginator.php';

/**
 * Description of CountryController
 *
 * @author student
 */
class CountryController extends BaseController {
    
    /** @var DAOCountry $daocity */
    
    private $daocountry;
    private $daocity;
    use Paginator;
    use CsrfToken;
    
    
    public function __construct() { 
        $this->daocountry = new DAOCountry(SingletonDataBase::getInstance()->cnx);
        $this->daocity = new DAOCity(SingletonDataBase::getInstance()->cnx);

    }
    
    public function test($alpha) {
        return $alpha;
        
    }
    
    public function show($id, $page = 1) {
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();

        }

        if(isset($_GET['p']) && !empty($_GET['p']) && isset($_GET['i']) && !empty($_GET['i'])){
            $page = (int) strip_tags($_GET['p']);
            $items_par_page = (int) strip_tags($_GET['i']);
        }else{
            $page = 1;
            $items_par_page = 5;
        }
        $start = (($page - 1)*$items_par_page);

        $country = $this->daocountry->find($id);
        $cities = $this->daocity->findAllByCountryID($id, $start, $items_par_page);
        $count = $this->daocity->countByCountryID($id);
        

        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();

       }
       // generer un token
       $csrf_token = $this->generateToken();
        $pagination = $this->paginate($uri, $page, $count, $items_par_page);
        $isLogged = Auth::isLogged();
        $page = Renderer::render('pays.php', compact('country', 'cities','pagination','csrf_token', 'isLogged'));
        echo $page;
        
        
    }
    
    public function All($continent){
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();

        }

        if(isset($_GET['p']) && !empty($_GET['p']) && isset($_GET['i']) && !empty($_GET['i'])){
            $page = (int) strip_tags($_GET['p']);
            $items_par_page = (int) strip_tags($_GET['i']);
        }else{
            $page = 1;
            $items_par_page = 5;
        }

        $start = (($page - 1)*$items_par_page);
        
        $isLogged = Auth::isLogged();
        $countries = $this->daocountry->findAllByContinent($continent, $start, $items_par_page);
        $count = $this->daocountry->countByContinent($continent);

       
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        $pagination = $this->paginate($uri, $page, $count, $items_par_page);
        $csrf_token = $this->generateToken();
        
        $isLogged = Auth::isLogged();
        $view = Renderer::render('liste_pays.php', compact('countries','continent','pagination','csrf_token', 'isLogged'));
        echo $view;  
    }
    
    public function showEdit($id){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();

       }
       // generer un token
       $csrf_token = $this->generateToken();

       // trouver les infos d'une ville
       $country = $this->daocountry->find($id);
       
       
       $page = Renderer::render('country_edit.php', compact("country","csrf_token"));
       echo $page;
    }
    
    public function doEdit($id){
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();

        }

        if(!$this->check('csrf_token')){
            echo 'error token';
        }else{
            $country = $this->daocountry->find($id);
            $country->setName(htmlspecialchars($_POST['name']));
            $country->setCode(htmlspecialchars($_POST['code']));
            $country->setContinent(htmlspecialchars($_POST['continent']));
            $country->setRegion(htmlspecialchars($_POST['region']));
            $country->setSurfaceArea(htmlspecialchars($_POST['SurfaceArea']));
            $country->setIndepYear(htmlspecialchars($_POST['IndepYear']));
            $country->setPopulation(htmlspecialchars($_POST['population']));
            $country->setLifeExpectancy(htmlspecialchars($_POST['LieExpectancy']));
            $country->setGNP(htmlspecialchars($_POST['GNP']));
            $country->setGNPOld(htmlspecialchars($_POST['GNPOld']));
            $country->setLocalName(htmlspecialchars($_POST['LOcalName']));
            $country->setGovernmentForm(htmlspecialchars($_POST['government']));
            $country->setHeadOfState(htmlspecialchars($_POST['HeadOfState']));
            $country->setCapital(htmlspecialchars($_POST['capital']));
            $country->setCode2(htmlspecialchars($_POST['code2']));
            $country->setImage1(htmlspecialchars($_POST['image1']));
            $country->setImage2(htmlspecialchars($_POST['image2']));


            $country = $this->daocountry->update($city);

            header('Location: /country/edit/'.$id);
            die();    
        }


    }
    
    public function showAdd(){
       if(session_status() != PHP_SESSION_ACTIVE){
            session_start();

       }
       // generer un token
       $csrf_token = $this->generateToken();
       $page = Renderer::render('country_add.php', compact('csrf_token'));
       echo $page;
    }
    
    public function doAdd(){
        
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();
        }

        if(!$this->check('csrf_token')){
            echo 'error token';
        }else{
            $country = new Country();
            $country->setName(htmlspecialchars($_POST['name']));
            $country->setCode(htmlspecialchars($_POST['code']));
            $country->setContinent(htmlspecialchars($_POST['continent']));
            $country->setRegion(htmlspecialchars($_POST['region']));
            $country->setSurfaceArea(htmlspecialchars($_POST['SurfaceArea']));
            $country->setIndepYear(htmlspecialchars($_POST['IndepYear']));
            $country->setPopulation(htmlspecialchars($_POST['population']));
            $country->setLifeExpectancy(htmlspecialchars($_POST['LieExpectancy']));
            $country->setGNP(htmlspecialchars($_POST['GNP']));
            $country->setGNPOld(htmlspecialchars($_POST['GNPOld']));
            $country->setLocalName(htmlspecialchars($_POST['LocalName']));
            $country->setGovernmentForm(htmlspecialchars($_POST['government']));
            $country->setHeadOfState(htmlspecialchars($_POST['HeadOfState']));
            $country->setCapital(htmlspecialchars($_POST['capital']));
            $country->setCode2(htmlspecialchars($_POST['code2']));
            $country->setImage1(htmlspecialchars($_POST['image1']));
            $country->setImage2(htmlspecialchars($_POST['image2']));

            $country = $this->daocountry->save($country);

            header('Location: /country/add');
            die();    
        }
    }
    
    public function delete($id){
        
        if(session_status() != PHP_SESSION_ACTIVE){
             session_start();

        }

        if(!$this->check('csrf_token')){
            echo 'error token';
        }else{
            $country = $this->daocountry->find($id);

            $country = $this->daocountry->remove($id);

            header("Location: {$_SERVER["HTTP_REFERER"]}");
            die();    
        }


    }
    
}