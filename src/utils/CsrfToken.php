<?php
/**
 * Description of CsrfToken
 *
 * @author student
 */
trait CsrfToken {

    private static $token_name = "csrf_token";
    
    public function generateToken() : string {
        $csrf_token = sha1(time()*rand(0,100));
        if(!isset($_SESSION[self::$token_name])){
            $_SESSION[self::$token_name] = $csrf_token;
        }
        return $_SESSION[self::$token_name];
    }
    
    public function check(string $key) : bool{
        if ($_SERVER["REQUEST_METHOD"] == "GET") {
            $methode = $_GET[$key];
        }else{
            $methode = $_POST[$key];
        }
        if($methode != $_SESSION[self::$token_name]){
            return false;
        }else{
            unset($_SESSION["csrf_token"]);
            return true;
        }
    }
    
}
