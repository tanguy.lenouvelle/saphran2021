<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Flash
 *
 * @author student
 */
trait Flash {
    
    public function setFlash(string $msg, string $color = ""){
        if(!isset($_SESSION['flash'])){
            $_SESSION['flash']['txt'] = $msg;
            $_SESSION['flash']['color'] = $color;
        }
    }
    
    public function flash($attr){

       if(isset($_SESSION['flash'])){
           $rs = $_SESSION['flash'];
           unset($_SESSION['flash']);
           return $rs[$attr];
       }

    }
    
}
