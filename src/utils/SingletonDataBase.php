<?php
require_once'SingletonConfigReader.php';




/**
 * Description of SIngletonDataBase
 *
 * @author student
 */
class SingletonDataBase {
    
    public $cnx;
    
    private static $serveur;
    private static $database;
    
    private static $username;
    private static $password;
    
    private static $instance=null;
    
    
    
    private function __construct() {
        
      self::$serveur = SingletonConfigReader::getInstance()->getValue('serveur','mariadb');
      self::$database = SingletonConfigReader::getInstance()->getValue('database','mariadb');
      self::$username = SingletonConfigReader::getInstance()->getValue('username','mariadb');
      self::$password = SingletonConfigReader::getInstance()->getValue('password','mariadb');
      
       $this->cnx = new PDO('mysql:host='.self::$serveur.';dbname='.self::$database, self::$username, self::$password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $this->cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

       }
    
    public function getInstance() : SingletonDataBase {
        if(self::$instance == null){
            self::$instance = new SingletonDataBase;
        }
        return self::$instance;
    }
    
}