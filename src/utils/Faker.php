<?php
require_once'SingletonDataBase.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Faker
 *
 * @author student
 */
class Faker {
    //put your code here

    protected $cnx;

        
    public function __construct() {
        $this->cnx= SingletonDataBase::getInstance()->cnx;

    }
    
    public function createRoles(){
         $SQL = array(
                 "req1"=> "INSERT INTO roles SET idrole = '1', nom = 'admin', permissions = '777'",
                 "req2"=> "INSERT INTO roles SET idrole = '2', nom = 'moderateur', permissions = '777'",
                 );
         foreach($SQL as $k=>$v){
            $preparedStatement = $this->cnx->prepare($SQL[$k]);
            $preparedStatement->execute();
         }
               
    }
    
    public function createUsers(){
        $password = rand(1231,15615); 
        $pwd = password_hash($password, PASSWORD_DEFAULT);
        
         $SQL = array(
                 "req1"=> "INSERT INTO users SET login = '1', passwd = {pwd}, idrole = '1'",
                 "req2"=> "INSERT INTO users SET login = '2', passwd = {pwd}, idrole = '2'",
                 );
         foreach($SQL as $k=>$v){
            $preparedStatement = $this->cnx->prepare($SQL[$k]);
            $preparedStatement->execute();
         }
        
    }
}

$ins = new Faker();
$ins->createUsers();