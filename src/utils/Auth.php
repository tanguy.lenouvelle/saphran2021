<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Auth{
    
    static $KEY="USERSESSION";
    static $CANDELETE=1;
    static $CANUPDATE=2;
    static $CANCREATE=4;
    
    /**
     * Store User Object in session 
     * @param User $user
     */
    
    public function login(User $user){
        
        
        if(!isset($_SESSION['user'])){
            $_SESSION['user']['login'] = $user->getLogin();
            $_SESSION['user']['passwd'] = $user->getPasswd();
        }
    }
    
    /**
     * Logout the current user (if logged)
     */
    public static function logout(){
        
    }
    
    /**
     * check if current connection belongs to a logged user 
     * @return bool
     */
    public static function isLogged() : bool{
        if(isset($_SESSION['user'])){
            return true;
        }
        return false;
    }
    
    /**
     * Check if user has a role
     * @param string $role
     * @return bool
     */
    public static function hasRole(string $role) : bool {
        
    }
    
    /**
     * Check if current user has required permission
     * @param int $perm
     * @return bool
     */
    public static function can(int $perm) : bool{
        return null;
    }
}
/*
if(Auth::can(Auth::$CANCREATE)){
    
}*/