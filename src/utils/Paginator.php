<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paginator
 *
 * @author student
 */
trait Paginator {

    /**
     * 
     * @param string $uri la base des urls pour generer les liens
     * @param int $page la page courante
     * @param int $total_items 
     * @param int $items_per_page (par defaut 10)
     */
    public function paginate(string $uri, int $page, int $total_items, int $items_per_page){

                    
        if($page > 1){
            $hasprevious  = "enabled";
        }else{
            $hasprevious = "disabled";
        }

        if($page == ceil($total_items/$items_per_page)){
            $hasnext  = "disabled";
        }else{
            $hasnext = "enabled";
        }   
        
        $href_next = $page+1;
        $href_previous = $page-1;
        

        
        $segment = <<<XXX
                <nav aria-label="Pagination">
                    <ul class="pagination">
                      <li class="page-item $hasprevious">
                          <a class="page-link" href="{$uri}?p={$href_previous}&i={$items_per_page}">Previous</a>
                      </li>
                
                      <li class="page-item $hasnext">
                          <a class="page-link" href="{$uri}?p={$href_next}&i={$items_per_page}">Next</a>
                      </li>
                    </ul>
                  </nav>


                XXX;
        
        return $segment;
    }
    
}
