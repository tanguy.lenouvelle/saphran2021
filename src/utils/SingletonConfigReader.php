<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingletonConfigReader
 *
 * @author student
 */
class SingletonConfigReader {
    
    private $config;
    private static $instance;
    
    
    private function __construct()
    {
        /*filter input utilisé pour ne pas acceder directement a la superglobale SERVER*/
        $this->config = parse_ini_file(filter_input(INPUT_SERVER,'DOCUMENT_ROOT').'/../config.ini', true);
    }
    
    public static function getInstance() : SingletonConfigReader
    {
        if (self::$instance == null)
        {
            self::$instance = new SingletonConfigReader();
        }
        return self::$instance;
    }
    
    
    
       
    public function getValue($key, $section=null) : ?string
    {
        if ($section != null)
        {
            try
            {
                return $this->config[$section][$key];
            }
            catch(Exception $e)
            {
                return null;
            }
            
        }
        else
        {
            return $this->config[$key];
        }
    }
    

 
    

}
